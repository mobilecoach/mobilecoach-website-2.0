---
id: publications
title: Publications
---
## 2024

Paersch, C., Recher, D.A., Schulz, A., Heininger, M., Schlup, B., Künzler, F., Homan, S., Kowatsch, T., Fisher, A.J., Horn, A., Kleim, B. (forthcoming) **Self-efficacy effects on symptom experiences in daily life and early treatment success in anxiety patients**, Clinical Psychological Science.

Müller-Bardorff, M., Schulz, A., Paersch, C., Recher, D.A., Schlup, B., Seifritz, E., Kolassa, I., Kowatsch, T., Fisher, A.J., Galatzer-Levy, I., Kleim, B. (2024) **Optimizing Outcomes in Psychotherapy for Anxiety Disorders (OPTIMAX): Study Protocol for a Randomized Controlled Trial on Efficacy and Response Prediction using smartphone-based and passive sensing features**, JMIR Research Protocols, [10.2196/42547](https://doi.org/10.2196/42547)

Schläpfer, S., Schneider, F., Santhanam, P., Eicher, M., Kowatsch, T., Witt, C., Barth, J. (2024) **Engagement with a Relaxation and Mindfulness Mobile App: Exploratory Analysis of Usage Data and Self-Reports from a Randomized Controlled Trial in People with Cancer**, JMIR Cancer, [10.2196/52386](https://doi.org/10.2196/52386).

Sou, D., Gruener, M.R., Bischof, A., Santhanam, P., Kowatsch, T., Nißen, M.K. (2024) **VOGUE: Influence of the Menstrual Cycle on Technology Usage**, 12th Annual International Society for Research on Internet Interventions (ISRII) Meeting, Limerick, Ireland, 2-6 June 2024, Abstract & Poster.

Cohen Rodrigues, T., de Buisonjé, D., Reijnders, T., Santhanam, P., Kowatsch, T., Breeman, L.D., Janssen, V., Kraaijenhagen, R., Atsma, D., Evers, A., (2024) **Human Cues in Ehealth to Promote Lifestyle Change: An Experimental Field Study to Examine Adherence to Self-Help Interventions, Internet Interventions**, Vol. 35 [10.1016/j.invent.2024.100726](https://www.sciencedirect.com/science/article/pii/S2214782924000198?via%3Dihub).

de Buisonjé, D., Reijnders, T., Cohen Rodrigues, T., Santhanam, P., Kowatsch, T., Breeman, L.D., Janssen, V., Kraaijenhagen, R., Kemps, H., Evers, A., (2024) **Less stick more carrot? Increasing the uptake of deposit contract financial incentives for physical activity: A randomized controlled trial**, Psychology of Sport & Exercise, 70, [10.1016/j.psychsport.2023.102532](https://doi.org/10.1016/j.psychsport.2023.102532).

Ulrich, S., Gantenbein, A.R., Zuber, V., Von Wyl, A., Kowatsch, T., Künzli, H., (2024) **Development and Evaluation of a Smartphone-Based Chatbot Coach to Facilitate a Balanced Lifestyle in Individuals With Headaches (BalanceUP App): Randomized Controlled Trial**, Journal of Medical Internet Research, Vol. 26, e50132, [10.2196/50132](https://www.jmir.org/2024/1/e50132).


## 2023

Olaru, G., van Scheppingen, M., Stieger, M., Kowatsch, T., Flückiger, C., Allemand, M. (2023) **The effects of a personality intervention on satisfaction in 10 domains of life: Evidence for increases and correlated change with personality traits, Journal of Personality and Social Psychology**, 125(4), 902–924 [10.1037/pspp0000474](https://doi.org/10.1037/pspp0000474).

Chan, W.S., Cheng, W.Y., Lok, S.H.C., Cheah, A.K.M., Lee, A.K.W., Ng, A.S.Y., Kowatsch, T. (2023) **How to Optimize Digital Cognitive Behavioral Therapy for Insomnia (dCBTi)? A Randomized Controlled Comparative Trial of dCBTi With Different Types of Coaching Support**, JMIR Preprints. 09/08/2023:51716 [10.2196/preprints.51716](https://doi.org/10.2196/preprints.51716).

Mishra, V., Künzler, F., Kramer, J., Fleisch, E., Kowatsch, T., Kotz, D.F. (2023) **Detecting Receptivity for mHealth Interventions, GetMobile: Mobile Computing and Communications**, 27 (2), 23–28, [10.1145/3614214.3614221](https://doi.org/10.1145/3614214.3614221).

Bierbauer, W., Bermudez, T., Bernardo, A., Fleisch-Silvestri, R., Hermann, M., Schmid, J.P., Kowatsch, T., Scholz, U. (2023) **Predicting physical activity following cardiac rehabilitation: A longitudinal observational study**, Rehabilitation Psychology. Advance online publication. [10.1037/rep0000490](https://doi.org/10.1037/rep0000490).

Lindner, S., Stieger, M., Rüegger, D., Kowatsch, T., Flückiger, C., Mehl, M.R., Allemand, M. (2023) **How Is Variety in Daily Life Related to the Expression of Personality States? An Ambulatory Assessment Study**, European Journal of Personality, [10.1177/08902070221149593](https://journals.sagepub.com/doi/10.1177/08902070221149593).

Ulrich, S., Lienhard, N., Künzli, H., Kowatsch, T., (2023) **Mindfulness Focused Stress Management Chatbot (MISHA) for Students: Pilot Randomized Controlled Trial**, JMIR Preprints, 04/12/2023:54945, [10.2196/preprints.54945](http://doi.org/10.2196/preprints.54945).

Ollier, J.B., Suryapalli, P., Fleisch, E., von Wangenheim, F., Mair, J., Salamanca-Sanabria, A., Kowatsch, T., (2023) **Can digital health researchers make a difference during the pandemic? Results of the single-arm chatbot-led Elena+: Care for COVID-19 interventional study**, Frontiers in Public Health, 11, [10.0.13.61/fpubh.2023.1185702](https://doi.org/10.3389/fpubh.2023.1185702).

Teepe, G., Lukic, Y., Kleim, B., Jacobson, N.C., Schneider, F., Santhanam, P., Fleisch, E., Kowatsch, T., (2023) **Development of a digital biomarker and intervention for subclinical depression: study protocol for a longitudinal waitlist control study**, BMC Psychology 11, 186. [10.1186/s40359-023-01215-1](https://doi.org/10.1186/s40359-023-01215-1).

Castro, O., Mair, J., Salamanca-Sanabria, A., Alattas, A., Keller, R., Zheng, S., Jabir, A., Lin, X., Frese, B., Lim, C.S., Santhanam, P., Van Dam, R., Car, J., Lee, J., Tai, E.S., Fleisch, E., von Wangenheim, F., Tudor Car, L., Müller-Riemenschneider, F., Kowatsch, T., (2023) **Development of “LvL UP 1.0”, a smartphone-based, conversational agent-delivered holistic lifestyle intervention for the prevention of non-communicable diseases and common mental disorders**, Frontiers in Public Health, 5, 2673-253X, [10.3389/fdgth.2023.1039171](https://doi.org/10.3389/fdgth.2023.1039171).


## 2022

Olaru, G., Stieger, M., Rüegger, D., Kowatsch, T., Flückiger, C., Roberts, B.W., Allemand, M. (2022) **Personality Change Through a Digital-Coaching Intervention: Using Measurement Invariance Testing to Distinguish Between Trait Domain, Facet and Nuance Change, European Journal of Personality**, Online First [10.1177/08902070221145088](https://doi.org/10.1177/08902070221145088).

de Buisonjé, D., Reijnders, T., Cohen Rodrigues, T., Santhanam, P., Kowatsch, T., Lipman, S.A., Bijmolt, T.H., Breeman, L.D., Janssen, V., Kraaijenhagen, R., Kemps, H., Evers, A. (2022) **Investigating Rewards and Deposit Contract Financial Incentives for Physical Activity Behavior Change Using a Smartphone App: Randomized Controlled Tria**l, Journal of Medical Internet Research (JMIR) 24(10):e38339, [10.2196/38339](https://doi.org/10.2196/38339).

Salamanca-Sanabria, A., Ollier, J.B., Mair, J., Kowatsch, T. (2022) **Feasibility of the Elena Care for COVID-19 Pandemic Lifestyle Care Intervention**, Presentation at the 11th Scientific Meeting of the International Society for Research on Internet Interventions (ISRII) Pittsburgh, PA, USA, Sep 18-21, 2022. 

Mair, J., Salamanca-Sanabria, A., Castro, O., Alattas, A., Keller, R., Kowatsch, T. (2022) **Feasibility of LvLUP: designing a smartphone-based chatbot-delivered lifestyle behaviour intervention**, Poster presented at the 11th Scientific Meeting of the International Society for Research on Internet Interventions (ISRII) Pittsburgh, PA, USA, Sep 18-21, 2022.

Bierbauer, W., Bermudez, T., Bernardo, A., Fleisch-Silvestri, R., Hermann, M., Schmid, J.P., Kowatsch, T., Scholz, U. (2022) **Implementation of a Novel Medication Regimen Following Cardiac Rehabilitation: an Application of the Health Action Process Approach**, International Journal of Behavioral Medicine, Advance online publication, 18 February 2022, [10.1007/s12529-022-10067-9](https://doi.org/10.1007/s12529-022-10067-9).

Skvortsova, A., Cohen Rodrigues, T., de Buisonjé, D., Kowatsch, T., Santhanam, P., Veldhuijzen, D., van Middendorp, H., Evers, A. (in press) (2022) **Increasing Effectiveness of a Physical Activity Smartphone Intervention With Positive Suggestions: Randomized Controlled Trial**, Journal of Medical Internet Research (JMIR) 29/04/2021:30057 [10.2196/32130](https://doi.org/10.2196/32130).

Nißen, M.K., Rüegger, D., Stieger, M., Flückiger, C., Allemand, M., von Wangenheim, F., Kowatsch, T. (in press) (2022) **The Effects of Healthcare Chatbot Personas with Different Social Roles on the Client-Chatbot Bond and Usage Intentions: Development of a Design Codebook and Web-based Experiment**, Journal of Medical Internet Research (JMIR) [10.2196/32630](https://doi.org/10.2196/32630).

Paz, R., Haug, S., Debelak, R., Jakob, R., Kowatsch, T., Schaub, M.P. (2022) **Engagement With a Mobile Phone–Based Life Skills Intervention for Adolescents and Its Association With Participant Characteristics and Outcomes: Tree-Based Analysis**, Journal of Medical Internet Research (JMIR) 2022;24(1):e28638, [10.2196/28638](https://doi.org/10.2196/28638).

Nißen, M.K., Selimi, D., Janssen, A., Rodríguez Cardona, D., Breitner, M.H., Kowatsch, T., von Wangenheim, F., (2022) **See you soon again, chatbot? A design taxonomy to characterize user-chatbot relationships with different time horizons**, Computers in Human Behavior 127(Feb 2022:10704), [10.1016/j.chb.2021.107043](https://doi.org/10.1016/j.chb.2021.107043).

## 2021

Ollier, J.B., Neff, S., Dworschak, C., Sejdiji, A., Santhanam, P., Keller, R., Xiao, G., Asisof, A., Rüegger, D., Bérubé, C., Hilfiker Tomas, L., Neff, J., Yao, J., Alattas, A., Varela-Mato, V., Pitkethly, A., Vara, M.D., Herrero, R., Baños, R.M., Parada, C., Agatheswaran, R.S., Villalobos, V., Keller, O., Chan, W.S., Mishra, V., Jacobson, N.C., Stanger, C., He, X., von Wyl, V., Weidt, S., Haug, S., Schaub, M.P., Kleim, B., Barth, J., Witt, C., Scholz, U., Fleisch, E., von Wangenheim, F., Tudor Car, L., Müller-Riemenschneider, F., Hauser-Ulrich, S., Núñez Asomoza, A., Salamanca-Sanabria, A., Mair, J., Kowatsch, T. (2021) **Elena Care for COVID-19, a Pandemic Lifestyle Care Intervention: Intervention Design and Study Protocol**, Frontiers in Public Health, 9(1543), [10.3389/fpubh.2021.625640](https://doi.org/10.3389/fpubh.2021.625640).

Kowatsch, T., Shih, I., Lukic, Y., Keller, O., Heldt, K., Durrer, D., Stasinaki, A., Büchter, D., Brogle, B., Farpour-Lambert, N., l’Allemand, D. (2021) **A Playful Smartphone-based Self-regulation Training for the Prevention and Treatment of Child and Adolescent Obesity: Technical Feasibility and Perceptions of Young Patients**, 1st Workshop on Healthy Interfaces (HEALTHI), collocated with the 26th ACM Annual Conference on Intelligent User Interfaces (IUI) – Where HCI meets AI, Virtually Hosted by Texas A&M University, April 13-17, 2021, College Station, USA. [researchgate.net/publication/349363769](https://www.researchgate.net/publication/349363769).

Stasinaki, A., Büchter, D., Shih, I., Heldt, K., Güsewell, S., Brogle, B., Farpour-Lambert, N., Kowatsch, T., l’Allemand, D. (2021) **Effects of a novel mobile health intervention compared to a multi-component behaviour changing program on body mass index, physical capacities and stress parameters in adolescents with obesity: a randomized controlled trial**, BMC Pediatrics 21 (308), [10.1186/s12887-021-02781-2](https://doi.org/10.1186/s12887-021-02781-2).

Mishra, V., Künzler, F., Kramer, J., Fleisch, E., Kowatsch, T., Kotz, D.F. (2021) **Detecting Receptivity for mHealth Interventions in the Natural Environment**, The Proceedings of the ACM on Interactive, Mobile, Wearable and Ubiquitous Technologies (IMWUT) 5(2), Article 74 (June 2021), [10.1145/3463492](https://doi.org/10.1145/3463492).

Cohen Rodrigues, T., Reijnders, T., de Buisonjé, D., Santhanam, P., Kowatsch, T., Janssen, V., Kraaijenhagen, R., Atsma, D., Evers, A. (2021) **Human Cues in Self-help Lifestyle Interventions: an Experimental Field Study**, Journal of Medical Internet Research (JMIR) Preprints. 29/04/2021:30057, [10.2196/preprints.30057](https://preprints.jmir.org/preprint/30057).

Fleisch, E., Franz, C., Herrmann, A., Mönninghoff, A. (2021) **Die digitale Pille: Eine Reise in die Zukunft unseres Gesundheitssystems**, Campus Verlag: Frankfurt a.M., Deutschland

Fleisch, E., Franz, C., Herrmann, A. (2021) **The Digital Pill: What Everyone Should Know about the Future of Our Healthcare System**, Emerald Publishing: Bingley, United Kingdom

Ollier, J.B., Santhanam, P., Kowatsch, T. (2021) **Face(book)ing the Truth: Initial Lessons Learned using Facebook Advertisements for the Chatbot-delivered Elena+ Care for COVID-19 Intervention**, In Proceedings of the 14th International Joint Conference on Biomedical Engineering Systems and Technologies (BIOSTEC 2021) – Volume 5: HEALTHINF, 781-788 ISBN: 978-989-758-490-9 ISSN: 2184-4305.

Kowatsch, T., Schachner, T., Harperink, S., Barata, F., Dittler, U., Xiao, G., Stanger, C., Oswald, H., Fleisch, E., von Wangenheim, F., Möller, A. (2021) **Conversational Agents as Mediating Social Actors in Chronic Disease Management Involving Healthcare Professionals, Patients, and Family Members: Intervention Design and Results from a Multi-site, Single-arm Feasibility Study**, Journal of Medical Internet Research (JMIR) 23(2):e25060, [10.2196/25060](https://doi.org/10.2196/25060).

Kowatsch, T., Fleisch, E. (2021) **Digital Health Interventions**, in: Gassmann, Oliver; Ferrandina, Fabrizio (eds): Connected Business: Creating Value in the Networked Economy, Springer: Berlin, New York [10.1007/978-3-030-76897-3_4](https://link.springer.com/chapter/10.1007/978-3-030-76897-3_4)

Stanger, C., Kowatsch, T., Xie, H., Nahum-Shani, I., Lim Liberty, F., Anderson, M., Santhanam, P., Kaden, S., Rosenberg, B. (2021) **SweetGoals, a Digital Health Intervention for Young Adults with Type 1 Diabetes: Protocol for a Factorial Randomized Trial**, JMIR Research Protocols 10(2):e27109 [10.2196/27109](https://doi.org/10.2196/27109).

Stieger M., Flückiger C., Rüegger D., Kowatsch T., Roberts B.W., Allemand M. (2021) **Changing personality traits with the help of a digital personality change intervention**, Proceedings of the National Academy of Sciences (PNAS) 118(8):e2017548118 [10.1073/pnas.2017548118](https://doi.org/10.1073/pnas.2017548118)

Schläpfer, S., Stanic, J., Eicher, M., Kowatsch, T., Witt, C., Barth, J. (2021) **Development of a mindfulness and relaxation app and evaluation of the effectiveness on cancer patients’ distress: a randomized controlled multicenter study**, Fachtagung “Chronisch krank in der digitalen Welt”, 13. January 2021 [Web](https://www.researchgate.net/publication/348266841)

de Buisonjé, D., Reijnders, T., Cohen Rodrigues, T., Keesman, M., Santhanam, P., Kowatsch, T., Janssen, V., Kraaijenhagen, R., Kemps, H., Evers, A. (2021) **Less carrot more stick: investigating deposit contract financial incentives for physical activity behavior change in a smartphone application**, 10th Annual Association for Researchers in Psychology and Health (ARPH) Conference 2021, 28 & 29 January 2021, [PDF](http://cocoa.ethz.ch/downloads/2021/01/2598_deBuisonje%20et%20al%202021%20ARPH%20PhysicalActivityIntervention.pdf)

Cohen Rodrigues, T., Reijnders, T., de Buisonjé, D., Santhanam, P., Kowatsch, T., Janssen, V., Kraaijenhagen, R., Atsma, D., Evers, A. (2021) **Human attributes in conversational agents: A field study with an app-based lifestyle intervention**, 10th Annual Association for Researchers in Psychology and Health (ARPH) Conference 2021, 28 & 29 January 2021, [PDF](http://cocoa.ethz.ch/downloads/2021/01/2593_CohenRodrigues%20et%20al%202021%20ARPH%20CA-lifestyle-intervention.pdf).


## 2020

Rassouli, F., Tinschert, P., Barata, F., Steurer-Stey, C., Fleisch, E., Puhan, M., Baty, F., Kowatsch, T., Brutsche, M. (2020) **Characteristics of asthma-related nocturnal cough – a potential new digital biomarker**, Journal of Asthma and Allergy 13, 649—657 [10.2147/JAA.S278119](https://doi.org/10.2147/JAA.S278119)

Tinschert, P., Rassouli, F., Barata, F., Steurer-Stey, C., Fleisch, E., Puhan, M., Kowatsch, T., Brutsche, M. (in press) **Nocturnal cough and sleep quality to assess asthma control and predict attacks**, Journal of Asthma and Allergy 13, 669-678 [10.2147/JAA.S278155](https://doi.org/10.2147/JAA.S278155)

Rüegger, D., Stieger, M., Nißen, M.K., Allemand, M., Fleisch, E., Kowatsch, T. (2020) **How Are Personality States Associated with Smartphone Data?**, European Journal of Personality 34(5) Special Issue: Behavioral personality science in the age of big data, 687-713 [10.1002/per.2309](https://doi.org/10.1002/per.2309)

Winkelbeiner, S., Sels, L., Homan, P., Klee, N., Santhanam, P., Vetter, S., Seifritz, E., Galatzer-Levy, I., Kowatsch, T., Scholz, U., Kleim, B. (2020) **The Potential of Ecological Momentary Assessments in the Prediction of Suicidal Ideation: A Feasibility Study**, Biological Psychiatry 87(9), Suppl, 1 May 2020, S451, [10.1016/j.biopsych.2020.02.1149](https://doi.org/10.1016/j.biopsych.2020.02.1149)

Rassouli, F., Tinschert, P., Barata, F., Steurer-Stey, C., Fleisch, E., Puhan, M., Baty, F., Kowatsch, T., Brutsche, M. (2020) **Characteristics of Asthma-related Nocturnal Cough – A Potential New Digital Biomarker**, European Respiratory Society International Congress (ERS 2020) (virtual), Barcelona, Spain, September 7-9, 2020. [Poster](http://cocoa.ethz.ch/downloads/2020/09/2554_ePoster2_1.pdf)

Tinschert, P., Rassouli, F., Barata, F., Steurer-Stey, C., Fleisch, E., Puhan, M., Baty, F., Kowatsch, T., Brutsche, M. (2020) **Nocturnal Cough and Sleep Quality to Assess Asthma Control and Predict Attacks**, European Respiratory Society International Congress (ERS 2020) (virtual), Barcelona, Spain, September 7-9, 2020. [Poster](http://cocoa.ethz.ch/downloads/2020/09/2555_ePoster3.pdf)

Barata, F., Tinschert, P., Rassouli, F., Brutsche, M., Kotz, D.F., Puhan, M., Fleisch, E., Kowatsch, T. (2020) **Automatic recognition, segmentation and sex assignment of nocturnal asthmatic cough and cough epochs in smartphone-based audio recordings: Results from an observational field study**, Journal of Medical Internet Research (JMIR) 22(7):e18082, [10.2196/18082](https://doi.org/10.2196/18082).

Stieger, M., Eck, M., Rüegger, D., Kowatsch, T., Flückiger, C., Allemand, M. (2020) **Who Wants to Become More Conscientious, More Extraverted, or Less Neurotic With the Help of a Digital Intervention**, Journal of Research in Personality [10.1016/j.jrp.2020.103983](https://doi.org/10.1016/j.jrp.2020.103983).

Hauser-Ulrich, S., Künzli, H., Meier-Peterhans, D., Kowatsch, T. (2020) **A Smartphone-based Healthcare Chatbot to Promote Self-Management of Chronic Pain (SELMA): A Pilot Randomized Control Trial**, JMIR Mhealth Uhealth 2020;8(4):e15806 [10.2196/15806](https://doi.org/10.2196/15806)

Stieger, M., Wepfer, S., Rüegger, D., Kowatsch, T., Roberts, B.W., Allemand, M. (2020) **Becoming More Conscientious or More Open to Experience? Effects of a Two-Week Smartphone-Based Intervention for Personality Change**, European Journal of Personality 34(3), 345-366, [10.1002/per.2267](https://dx.doi.org/10.1002/per.2267).

Haug, S., Paz, R., Scholz, U., Kowatsch, T., Schaub, M.P., Radtke, T. (2020) **Assessment of the Efficacy of a Mobile Phone-Delivered Just-in-Time Planning Intervention to Reduce Alcohol Use in Adolescents: Randomized Controlled Crossover Trial**, JMIR mHealth and uHealth (JMU) 8(5):e16937 [10.2196/16937](https://doi.org/10.2196/16937).

Kramer, J., Künzler, F., Mishra, V., Smith, S.N., Kotz, D.F., Scholz, U., Fleisch, E., Kowatsch, T. (2020) **Which Components of a Smartphone Walking App Help Users to Reach Personalized Step Goals? Results from an Optimization Trial**, Annals of Behavioral Medicine 54(7), 518–528 [10.1093/abm/kaaa002](https://doi.org/10.1093/abm/kaaa002).

Boateng, G., Lüscher, J., Scholz, U., Kowatsch, T. (2020) **Emotion Capture among Real Couples in Everyday Life**, 1st Momentary Emotion Elicitation & Capture (MEEC) workshop, co-located with the ACM CHI Conference on Human Factors in Computing Systems, Honolulu, Hawaii, USA, April 25th, 2020. [Web][2020_emotion_couples_field]

Boateng, G., Sels, L., Kuppens, P., Lüscher, J., Scholz, U., Kowatsch, T. (2020) **Emotion Elicitation and Capture among Real Couples in the Lab**, 1st Momentary Emotion Elicitation & Capture (MEEC) workshop, co-located with the ACM CHI Conference on Human Factors in Computing Systems, Honolulu, Hawaii, USA, April 25th, 2020. [Web][2020_emotion_couples_lab]

Presset, B., Kramer, J., Kowatsch, T., Ohl, F. (2020) **The social meaning of steps: User reception of a mobile health intervention on physical activity**, Critical Public Health. **[Web][2020_cph]**

## 2019

Boateng, G., Santhanam, P., Lüscher, J., Scholz, U., Kowatsch, T. (2019) **VADLite: An Open-Source Lightweight System for Real-Time Voice Activity Detection on Smartwatches**, 4th International Workshop on Mental Health: Sensing & Intervention, co-located with the 2019 ACM International Joint Conference on Pervasive and Ubiquitous Computing (UbiComp), London, UK. **[Web][19_vad_ubicomp]**

Kowatsch, T., Harperink, S., Dittler, U., Xiao, G., Stanger, C., Oswald, H., Möller, A. (2019) **A digital assistant for healthcare providers targeting 10 to 15-year-old patients with asthma and their family: results from a pilot study**, Abstract published by the Center for Digital Health Interventions, ETH Zurich & University of St.Gallen. **[Web][19_max_abstract]**

Lüscher, J., Kowatsch, T., Boateng, G., Santhanam, P., Bodemann, G., Scholz, U. (2019) **Social Support and Common Dyadic Coping in Couples’ Dyadic Management of Type II Diabetes: Protocol for an Ambulatory Assessment Application**, JMIR Res Protoc 2019;8(10):e13685 DOI: 10.2196/13685. **[Web][19_jmir_res_dymand]**

Künzler, F., Mishra, V., Kramer, J., Kotz, D.F., Fleisch, E., Kowatsch, T. (2019) **Exploring the State-of-Receptivity for mHealth Interventions**, Proceedings of the ACM on Interactive, Mobile, Wearable and Ubiquitous Technologies (IMWUT) 3(4): Paper 140. **[Web][19_acm_imwut_mhealth_sor]**

Boateng, G., Santhanam, P., Lüscher, J., Scholz, U., Kowatsch, T. (2019) **Poster: DyMand – An Open-Source Mobile and Wearable System for Assessing Couples’ Dyadic Management of Chronic Diseases**, The 25th Annual International Conference on Mobile Computing and Networking (MobiCom), Poster & Demo Paper, Los Cabos, Mexico. **[Web][19_acm_mobicom]**

Künzler, F. (2019) **Context-aware notification management systems for just-in-time adaptive interventions**, IEEE International Conference on Pervasive Computing and Communications Workshops (PerCom Workshops). IEEE, 2019, Kyoto, Japan. **[PDF][19_percom_optimax]**

Barata, F., Kipfer, K., Weber, M., Tinschert, P., Fleisch, E., Kowatsch, T. (2019) **Towards Device-Agnostic Mobile Cough Detection with Convolutional Neural Networks**, 2019 IEEE International Conference on Healthcare Informatics (ICHI), Xi’an, China, June 10-13. **[PDF][19_ichi_cough]**

Boateng, G., Santhanam, P., Lüscher, J., Scholz, U., Kowatsch, T. (2019) **DyMand: An Open-Source Mobile and Wearable System for Assessing Couples’ Dyadic Management of Chronic Diseases**, 14th International Conference on Design Science Research in Information Systems and Technology (DESRIST), June 4-6, Worcester, MA, USA. **[PDF][19_desrist_dymand]**

Kramer, J., Künzler, F., Mishra, V., Presset, B., Smith, S.N., Scholz, U., Kotz, D.F., Kowatsch, T. (2019) **Investigating Intervention Components and Exploring States of Receptivity for a Smartphone App to Promote Physical Activity: Protocol of a Microrandomized Trial**, JMIR Research Protocols, 8(1), e11540. **[PDF][19_jmir_1]**

Tinschert, P., Rassouli, F., Barata, F., Steurer-Stey, C., Fleisch, E., Puhan, M., Brutsche, M., Kowatsch, T. (2019) **Prevalence of Nocturnal Cough in Asthma and its Potential as a Marker for Asthma Control (MAC) in Combination with Sleep Quality: Protocol of a Smartphone-based, Multi-Centre, Longitudinal Observational Study with Two Stages**, BMJ Open. **[PDF][19_bmj_mac]**

Kowatsch, T., D. Fischer-Taeschler, F. Putzing, P. Bürki, C. Stettler, G. Chiesa-Tanner and E. Fleisch (2019) **Die digitale Pille für chronische Krankheiten**, in: Digitale Transformation von Dienstleistungen im Gesundheitswesen, M. Pfannstiel, P. Da-Cruz and H. Mehlich (eds.), Springer, Heidelberg, Germany. **[Web][19_springer_digital_pill]**

Tinschert, P., Barata, F., Kramer, J., Rassouli, F., Steurer-Stey, C., Puhan, M., Brutsche, M., Kowatsch, T. (2019) **Don’t Lose Heart: Preliminary Engagement Results in an Ecological Momentary Assessment (EMA) Study Evaluating Digital Biomarkers for Asthma**, Abstract presented at the International Society for Research on Internet Interventions (ISRII) Meeting 2019, Auckland, New Zealand. **[PDF][ISRII19_asthma]**

Kramer, J., Künzler, F., Tinschert, P., Kowatsch, T. (2019) **Trajectories of Engagement with a Digital Physical Activity Coach: Secondary Analysis of a Micro-Randomized Trial**, Abstract presented at the International Society for Research on Internet Interventions (ISRII) Meeting 2019, Auckland, New Zealand. **[PDF][ISRII19_ally]**

## 2018

Boateng, G., Santhanam, P., Lu&#776;scher, J., Scholz, U., Kowatsch, T. (2018) Poster: **Multimodal Affect Detection among Couples for Diabetes Management**. Black in AI Workshop, Neural Information Processing Systems
Conference, (NIPS) 2018. **[PDF][18_NeurIPS_dymand]**

Buhmann, J., Felix, J., Gächter, T., Kowatsch, T., Lehmann, R., von Lutterotti, N., Schedler, K., Steurer, J., Wolfrum, C. (2018) **Digitalisierung der Medizin: Konsequenzen für die Ausbildung**, Schweizerische Ärztezeitung 99(42), 1441-1444. **[PDF][18_SAZ]**

Stieger, M., Nißen, M.K., Rüegger, D., Kowatsch, T., Flückiger, C., Allemand, M. (2018) **PEACH, a smartphone- and conversational agent-based coaching intervention for intentional personality change: study protocol of a randomized, wait-list controlled trial**, BMC Psychology 6(43), 1-15. **[PDF][18_BMCP_PEACH]**

Kowatsch, T., M. K. Nißen, D. Rüegger, M. Stieger, C. Flückiger, M. Allemand and F. von Wangenheim (2018) **The Impact of Interpersonal Closeness Cues in Text-based Healthcare Chatbots on Attachment Bond and the Desire to Continue Interacting: An Experimental Design** In Proceedings of the 26th European Conference on Information Systems (ECIS 2018), Portsmouth, UK. **[PDF][18_ECIS]**

Shih, I., Nißen, M.K., Büchter, D., Durrer, D., l’Allemand, D., Fleisch, E., Kowatsch, T., **Smartphone-based Biofeedback Breathing Training for Stress Management**, Poster presented at the Applied Maschine Learning Days, EPFL, Lausanne, Switzerland. **[PDF][18_appML_1]**

Barata, F., Tinschert, P., Rassouli, F., Baty, F., Brutsche, M., Steurer-Stey, C., Puhan, M., Fleisch, E., Kowatsch, T., **Smartphone-based Cough and Sleep Quality Detection**, Poster presented at the Applied Machine Learning Days, EPFL, Lausanne, Switzerland. **[PDF][18_appML_2]**

Haug, S. & Paz, R., Eggli, P. & Schaub, M. (2018) **Drinking Goal Trajectories and Their Association with Client Characteristics and Outcomes among Clients in Outpatient Alcohol Treatment**, Substance Use & Misuse 53(10):1-12. **[PDF][18_SUMU_Haug]**

Haug, S., Paz, R., Wenger, A. & Schaub M. P. (2018) **Efficacy of a mobile phone-based life-skills training program for substance use prevention among adolescents: study protocol of a cluster-randomised controlled trial**, BMC Public Health 18:1102. **[PDF][18_BMCPH_Haug]**

Haug, S. & Paz, R. (2018) **Erreichbarkeit Jugendlicher für ein mobiltelefonbasiertes Programm zur Suchtprävention durch die Förderung von Lebenskompetenzen**, Sucht 64(3), 129-139. **[Web][18_SUCHT]**

## 2017

Kowatsch, T., Barata, F., Tinschert, P., Dittler, U., Egger, J.M., Meyer, F., Schaub, M., Fleisch, E., Oswald, H., Möller, A., **Digital Health Literacy Intervention for Children with Asthma**, Poster presented at the CSS Health Insurance meets CDHI Event, Lucerne, Switzerland. **[PDF][17_CSS_CDHI_CWA]**

Paz, R., Haug, S., Filler, A., Kowatsch, T., Schaub, M.P. (2017) **Engagement within a Mobile Phone-based Smoking Cessation Intervention for Adolescents and Its Association with Participant Characteristics and Outcomes**, Journal of Medical Internet Research (JMIR) 19(10):e356. **[PDF][17_JMIR_ENGA]**

Künzler, F., Kramer, J., Kowatsch, T. (2017) **Efficacy of Mobile Context-aware Notification Management Systems: A Systematic Literature Review and Meta-Analysis**, 2017 IEEE 13th International Conference on Wireless and Mobile Computing, Networking and Communications (WiMob), Rome, 2017, 131-138. doi: 10.1109/WiMOB.2017.8115839 **[PDF][17_PERCAM]**

Haug, S., Paz, R., Meyer, C., Filler, A., Kowatsch, T., Schaub, M.P. (2017) **A Mobile Phone-Based Life Skills Training Program for Substance Use Prevention Among Adolescents: Pre-Post Study on the Acceptance and Potential Effectiveness of the Program Ready4life**, JMIR Mhealth Uhealth 5(10):e143. **[PDF][17_JMU_r4l]**

Kowatsch, T., Nißen, M.K., Shih, I., Rüegger, D., Volland, D., Filler, A., Künzler, F., Barata, F., Haug, S., Büchter, D., Brogle, B., Heldt, K., Gindrat, P., Farpour-Lambert, N., l’Allemand, D. (2017) **Text-based Healthcare Chatbots Supporting Patient and Health Professional Teams: Preliminary Results of a Randomized Controlled Trial on Childhood Obesity**, Persuasive Embodied Agents for Behavior Change (PEACH 2017) Workshop, co-located with the 17th International Conference on Intelligent Virtual Agents (IVA 2017), Stockholm, Sweden. **[PDF][17_peach]**

Haug, S., Paz, R., Kowatsch, T., Filler, A., Schaub, M.P. (2017) **Efficacy of a Technology-based Integrated Smoking Cessation and Alcohol Intervention for Smoking Cessation in Adolescents: Results of a Cluster-randomised Controlled Trial**, Journal of Substance Abuse Treatment 82(11), 55-66. **[PDF][17_jsat]**

Kowatsch, T., Wahle, F., Filler, A. (2017) **Design and Lab Experiment of a Stress Detection Service based on Mouse Movements**, The 11th Mediterranean Conference on Information Systems (MCIS), Genoa, Italy **Best Paper Award [PDF][17_mcis_stressOUT]**

Kowatsch, T., Volland, D., Shih, I., Rüegger, D., Künzler, F., Barata, F., Filler, A., Büchter, D., Brogle, B., Heldt, K., Gindrat, P., Farpour-Lambert, N., l’Allemand, D. (2017) **Design and Evaluation of a Mobile Chat App for the Open Source Behavioral Health Intervention Platform MobileCoach**, In: Maedche A., vom Brocke J., Hevner A. (eds) Designing the Digital Transformation. DESRIST 2017. Lecture Notes in Computer Science, vol 10243. Springer: Berlin; Germany, 485-489. **[Paper][17_desrist_paper] [Poster][17_desrist_paper] [Screencast][17_desrist_clip]**

Haug, S., Paz, R., Kowatsch, T., Filler, A., Dickson-Spillmann, M., Dey, M., Schaub, M.P. (2017) **Efficacy of a web- and text messaging-based intervention to reduce problem drinking in adolescents: Results of a cluster-randomised controlled trial**, Journal of Consulting and Clinical Psychology, 85(2),147-159. **Wilhelm Feuerlein Award 2018** **[PDF][17_jccp]**

Paz, R., Haug, S., Kowatsch, T., Filler, A., Schaub, M.P. (2017) **Moderators of Outcome in a Technology-based Intervention to Prevent and Reduce Problem Drinking Among Adolescents**, Addictive Behaviors 72: 64-71. **[PDF][17_ab_paz]**

Kowatsch, T., Wahle, F., Filler, A. (2017) **stressOUT: Design, Implementation and Evaluation of a Mouse-based Stress Management Service**, In: Designing the Digital Transformation: DESRIST 2017 Research in Progress Proceedings, Maedche, A., vom Brocke, J., Hevner, A. (eds), KIT Scientific Working Papers; 64, Karlsruhe, Germany, 37-45. **Nominee for the Best Research-in-Progress [PDF][17_desrist_stressOUT]**

Tinschert, P., Barata, F., Kowatsch, T. (2017) **Enhancing Asthma Control through IT: Design, Implementation and Planned Evaluation of the Mobile Asthma Companion**, in Leimeister, J.M.; Brenner, W. (Hrsg.): Proceedings der 13th International Conference on Wirtschaftsinformatik (WI 2017), St. Gallen, 1291-1294. **[PDF][17_wi_paper] [Video][17_wi_video]**

Künzler, F., Kramer, J., Mishra, V., Presset, B., Smith, S.N., Kotz, D.F., Scholz, U., Fleisch, E., Kowatsch, T., **Ally: A Smartphone-based Physical Activity Intervention**, Poster presented at the CSS Health Insurance meets CDHI Event, Lucerne, Switzerland. **[PDF][17_ally_cdhi_css]**

Shih, I., Volland, D., Rüegger, D., Künzler, F., Barata, F., Filler, A., Büchter, D., Brogle, B., Heldt, K., Gindrat, P., Farpour-Lambert, N., Fleisch, E., l’Allemand, D., Kowatsch, T., **Therapy Adherence of Obese Children in a 6-Month High-Frequency Intervention**, Poster presented at the CSS Health Insurance meets CDHI Event, Lucerne, Switzerland. **[PDF][17_iris_cdhi_css]**

## 2016

Barata, F., Kowatsch, T., Tinschert, P., Filler, A. (2016). **Personal MobileCoach: Tailoring Behavioral Interventions to the Needs of Individual Participants.** UBICOMP 2016 Workshop Designing, Developing, and Evaluating The Internet of Personal Health (IoPH), Heidelberg, Germany. **[PDF][16_personal_mc]**

## 2015

Kowatsch, T., Wahle, F., Filler, A., Kehr, F., Volland, D., Haug, S., Jenny, G., Bauer, G., Fleisch, E., (2015) **Towards Short-Term Detection of Job Strain in Knowledge Workers with a Minimal-Invasive Information System Service: Theoretical Foundation and Experimental Design**, 23rd European Conference on Information Systems (ECIS), Münster, Germany. **[Paper-PDF][2015_ecis_job_strain_paper]** **[Poster-PDF][2015_ecis_job_strain_poster]**

Haug, S., Paz, R., Kwon, M., Filler, A., Kowatsch, T., Schaub, M.P. (2015). **Smartphone use and Smartphone addiction among young people in Switzerland.** Journal of Behavioural Addictions 4(4), pp. 299-307. **[PDF][15_beh_addictions]**

Filler, A., Kowatsch, T., Haug, S., Wahle, F., Staake, T. & Fleisch, E. (2015). **MobileCoach: A Novel Open Source Platform for the Design of Evidence-based, Scalable and Low-Cost Behavioral Health Interventions - Overview and Preliminary Evaluation in the Public Health Context.** Wireless Telecommunications Symposium 2015 (WTS 2015), New York, USA. **[PDF][15_wts]**

## 2014

Haug, S., Paz Castro, R., Filler, A., Kowatsch, T., Fleisch, E. & Schaub, M.P. (2014). **Efficacy of an internet and SMS-based integrated smoking cessation and alcohol intervention for smoking cessation in young people: study protocol of a two-arm cluster randomised controlled trial.** BMC Public Health, 14: 1140. **[PDF][14_bmc_publichealth_mct]**

Filler, A., Haug, S. and Kowatsch, T. (2014). **The MobileCoach – An Open Source Solution for Behavioral Change Interventions.** Abstract presented at the 7th Scientific Meeting of The International Society for Research on Internet Interventions (ISRII), Valencia, Spain. **[PDF][14_isrii_abstract]**

Kowatsch, T., Wahle, F., Filler, A. and Fleisch, E. (2014) **Predicting Adverse Behavior with Early Warning Health Information Systems by Mining Association Rules on Multi-dimensional Behavior: A Proposal.** Poster presented at the 7th Scientific Meeting of The International Society for Research on Internet Interventions (ISRII), Valencia, Spain. **[PDF][14_isrii_poster]**

Haug, S., Kowatsch, T., Paz Castro, R., Filler, A. and Schaub, M.P. (2014) **Efficacy of a web- and text messaging-based intervention to reduce problem drinking in young people: study protocol of a cluster-randomised controlled trial.** BMC Public Health, 14: 809. **[PDF][14_bmc_publichealth_mca]**

## 2013

Haug, S. (2013). **Mobile phone text messaging to reduce alcohol and tobacco use in young people – a narrative review.** Smart Homecare Technology and TeleHealth, 1(1), 11-19. **[PDF][13_mobilephone]**

Haug, S. Bitter, G., Hanke, M., Ulbricht, S. Meyer, C. & John, U. (2013). **Kurzintervention zur Förderung der Tabakabstinenz via Short Message Service (SMS) bei Auszubildenden an beruflichen Schulen: Longitudinale Interventionsstudie zur Ergebnis- und Prozessevaluation.** Das Gesundheitswesen, 75(10), 625-631.

Haug, S., Schaub, M.P., Venzin, V. Meyer, C. & John, U. (2013). **Differenzielle Wirksamkeit eines Short Message Service (SMS)-basierten Programms zur Förderung des Rauchstopps bei Jugendlichen.** Psychiatrische Praxis, 40(6), 339-346.

Haug, S., Schaub, M.P., Venzin, V., Meyer, C., John, U. & Gmel, G. (2013). **A pre-post study on the appropriateness and effectiveness of a web- and text messaging-based intervention to reduce problem drinking in emerging adults.** Journal of Medical Internet Research, 15(9), e196. **[PDF][13_alkcheck]**

Haug, S., Schaub, M.P., Venzin, V., Meyer, C. & John, U. (2013). **Efficacy of a text message-based smoking cessation intervention for young people: a cluster randomized controlled trial.** Journal of Medical Internet Research, 15(8), e171. **[PDF][13_mainoutcome]**

## 2012

Haug, S., Venzin, V. & Meyer, C. (2012). **Förderung des Rauchstopps an Berufsfachschulen via SMS.** SuchtMagazin, 38(3/4), 38-42.

[2020_selma]: https://doi.org/10.2196/15806
[2020_ally]: https://doi.org/10.1093/abm/kaaa002
[2020_cph]: https://www.tandfonline.com/doi/full/10.1080/09581596.2020.1725445
[2020_emotion_couples_field]: https://www.researchgate.net/publication/339774683
[2020_emotion_couples_lab]: https://www.researchgate.net/publication/339774741

[19_ichi_cough]: https://www.researchgate.net/publication/333843771
[19_springer_digital_pill]: https://www.researchgate.net/publication/335306006
[19_jmir_res_dymand]: https://doi.org/10.2196/13685
[19_vad_ubicomp]: https://dl.acm.org/doi/abs/10.1145/3341162.3346274
[19_acm_mobicom]: https://dl.acm.org/doi/abs/10.1145/3300061.3343399
[19_max_abstract]: https://www.researchgate.net/publication/336278467
[19_desrist_dymand]: http://cocoa.ethz.ch/downloads/2019/05/2496_DESRIST%202019%20Final%20v2.pdf
[19_acm_imwut_mhealth_sor]: https://dl.acm.org/doi/abs/10.1145/3369805
[19_bmj_mac]: http://cocoa.ethz.ch/downloads/2019/01/2347_e026323.full.pdf
[19_jmir_1]: http://cocoa.ethz.ch/downloads/2019/02/2442_Kramer%20et%20al.%20(2019)%20-%20Investigating%20Intervention%20Components.pdf
[19_percom_optimax]: https://h-suwa.github.io/percomworkshops2019/papers/p435-kunzler.pdf
[ISRII19_ally]: https://www.researchgate.net/publication/328432000
[ISRII19_asthma]: https://www.researchgate.net/publication/328431910

[18_appML_1]: http://cocoa.ethz.ch/downloads/2017/12/None_171204-CDHI-SBBT-IS-v3.pdf
[18_appML_2]: http://cocoa.ethz.ch/downloads/2017/12/None_180127-30-AMLDays-AsthmaCoughSleep-Detection-FB-v2.pdf
[18_NeurIPS_dymand]: https://www.researchgate.net/publication/329782236_Multimodal_Affect_Detection_among_Couples_for_Diabetes_Management
[18_SAZ]: https://www.researchgate.net/publication/328346317
[18_SUMU_Haug]: https://www.researchgate.net/publication/324516042
[18_BMCPH_Haug]: https://www.researchgate.net/publication/327566096
[18_SUCHT]: https://www.researchgate.net/publication/327469973
[18_BMCP_PEACH]: https://www.researchgate.net/publication/327427955
[18_ECIS]: https://www.researchgate.net/publication/325391504

[2015_ecis_job_strain_paper]:http://cocoa.ethz.ch/downloads/2015/04/None_ECIS2015-JS-ISS-WIP-camera-ready-1.pdf
[2015_ecis_job_strain_poster]:http://cocoa.ethz.ch/downloads/2015/06/None_Kowatsch%20et%20al%202015-JSISS-Poster-v1.pdf

[17_iris_cdhi_css]: http://cocoa.ethz.ch/downloads/2017/12/None_171204-CDHI-MCClient-PM2-TK-v1.pdf
[17_ally_cdhi_css]: http://cocoa.ethz.ch/downloads/2017/12/None_171204-CDHI-Ally-FK-JNK.pdf
[17_CSS_CDHI_CWA]: http://cocoa.ethz.ch/downloads/2017/12/None_171204-CDHI-Asthma-DHLI-Study-PT-v2.pdf
[17_JMIR_ENGA]: https://www.researchgate.net/publication/320161531
[17_PERCAM]: https://www.researchgate.net/publication/320161660
[17_JMU_r4l]: http://cocoa.ethz.ch/media/documents/2017/10/2374_Haug%20et%20al%202017%20Ready%204%20Life%20-%20Pre%20Post%20Test.pdf
[17_peach]: http://cocoa.ethz.ch/downloads/2017/07/2368_Kowatsch%20et%20al%202017%20-%20THCB%20PEACH%20Workshop.pdf
[17_mcis_stressOUT]: http://cocoa.ethz.ch/downloads/2017/08/2216_Kowatsch%20et%20al.%202017%20Stress%20Detection%20via%20Mouse%20Movements%20in%20the%20Lab.pdf
[17_jsat]: http://cocoa.ethz.ch/downloads/2017/09/2354_Haug%20et%20al%202017%20-%20Integrated%20MobileCoach%20Alcohol%20and%20Tobacco%20Intervention.pdf
[17_desrist_paper]: http://cocoa.ethz.ch/downloads/2017/05/2338_Kowatsch%20et%20al%202017%20MobileCoach%20Chat%20App.pdf
[17_desrist_poster]: http://www.c4dhi.org/wp-content/uploads/2017/05/DESRIST-2017-MobileCoach-ChatApp-Poster-v1.pdf
[17_desrist_clip]: https://youtu.be/O-Tq3g8Z5YY
[17_desrist_stressOUT]: http://cocoa.ethz.ch/downloads/2017/05/2352_Kowatsch%20et%20al%202017%20stressOUT.pdf
[17_jccp]: http://cocoa.ethz.ch/downloads/2017/02/2248_Haug%20et%20al%202017%20SMS%20Intervention%20to%20Reduce%20Problem%20Drinking.pdf
[17_wi_paper]: http://cocoa.ethz.ch/downloads/2017/02/2273_Tinschert%20et%20al%20-%20MAC%40WI2017.pdf
[17_wi_video]: https://youtu.be/tNuME48omc0
[17_ab_paz]: http://cocoa.ethz.ch/media/documents/2017/05/2350_Castro%20et%20al%202017%20MobileCoach%20Problem%20Drinking%20-%20Moderators.pdf

[16_personal_mc]: home:Personal_Mobile_Coach.pdf

[15_beh_addictions]: http://cocoa.ethz.ch/downloads/2016/03/2218_doi_10.1556_2006.4.2015.037.pdf
[15_wts]: http://cocoa.ethz.ch/downloads/2015/06/2120_WTS-2015-MobileCoach-final_2.pdf

[14_isrii_abstract]: home:ISRII14_MCAbstract_FillerHaugKowatsch_v3.pdf
[14_isrii_poster]: home:ISRII14_EW_HIS_KowatschWahleFillerFleisch_Poster.pdf
[14_bmc_publichealth_mca]: home:14_Study_Protocol_MobileCoach_Alcohol_BMC_PUBLIC_HEALTH.pdf	
[14_bmc_publichealth_mct]: home:14_Study_Protocol_MobileCoachTobacco_BMC_PUBLIC_HEALTH.pdf

[13_alkcheck]: home:13_Alk_Check_JMIR.pdf
[13_mainoutcome]: home:13_Main_Outcome_SMS_Coach_JMIR.pdf
[13_mobilephone]: home:13_Review_SMS_interventions_SMART_HOMECARE_TECH.pdf
