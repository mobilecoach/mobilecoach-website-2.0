---
id: escalationmechanism
title: Escalation mechanism
---

This page explains how escalation messages can be created and sent out after a period of inactivity. This guide is for interventions that coach the participant in multiple sessions. But in general, the core principles remain the same and the mechanism can be extended for any MobileCoach based intervention. 

# 1-hour reminder on MobileCoach

**Context:** The patient started the intervention but did not finish it after one hour. After this period, the escalation mechanism kicks in, and the patient gets a reminder on the phone. This reminder does not appear in the chat.

However, some notifications are sent out when the conversational agent wants to communicate with the patient. These notifications appear as push notifications and are also then written in the chat.

## Variables Widget

Create the following variables

```
$newSessionStartedMinuteOfHour
$newSessionStartedHourOfDay
$newSessionStartedMonth
$newSessionStartedDayOfMonth
$newSessionStartedYear
$participantInactiveInMinutes
$platform -> value = notSet
$reminderAfter1hSent
$reminderAfter1hFrequency
$Session1Q1_permission
```

NOTE: To set the `$platform` variable to `notSet` you need to click on the `Edit` button. This should look like this: 

![Set platform to notSet](/img/set-notSet.png 'Set platform to notSet')

## Micro Dialog Widget

### Step 1: Enable the push notification on iOS devices 

For Android device the authorisation to send push notifications is triggered automatically and does not require any extra effort of the Intervention team. However, on iOS this conversational turn needs to be added on day 1 (`Session 1`) of the intervention.

![es-enable-push](/img/es-enable-push.png 'es-enable-push')

Please add the following text: 

```
One important aspect ☝️
---
Please accept the following request that allows me to send you push notifications 🙏
---
We promise not to spam 😌
```

like this: 
![es-enable-push-message](/img/es-enable-push-message.png 'es-enable-push-message')

The overall conversation turn should look like this:
![es-enable-push-message-turn](/img/es-enable-push-message-turn.png 'es-enable-push-message-turn')

The `select one` text being:

```
All right 👍:1
Wouldn't like to do that 😕:2
```

The answer to this question is saved into the variable `$Session1Q1_permission`.

One important thing is not to forget about setting the rule on the bottom of this page to: 
![es-platform-ios-condition](/img/es-platform-ios-condition.png 'es-platform-ios-condition')
<!--- ![Screenshot 2020-11-13 at 22.04.36](/Users/dcleres/Library/Application%20Support/typora-user-images/Screenshot%202020-11-13%20at%2022.04.36.png) --->

If you want, you can also add an additional conversational turn to convince people to enable the push notification if they refused it in the first place. This could contain the following text: 

```
But without these messages you will miss our sessions!
---
That would be a great pity 😣
```

The answer would of type `select one` and could be something like: 

```
Okay, then. 👍:1
```

Then the rules would be the following:

![es-enable-push-message-rule](/img/es-enable-push-message-rule.png 'es-enable-push-message-rule')

![es-enable-push-message-rule1](/img/es-enable-push-message-rule1.png 'es-enable-push-message-rule1')

![es-enable-push-message-rule2](/img/es-enable-push-message-rule2.png 'es-enable-push-message-rule2')

![es-enable-push-message-rule3](/img/es-enable-push-message-rule3.png 'es-enable-push-message-rule3')



Finally, you can add the conversational turn that will ask for permission on iOS systems. The text part should contain:

```request-push-permissions
request-push-permissions
```

Please, make sure to tick the box called `This message is a command (invisible for participant)`

This should look like this:
![es-req-push-permission](/img/es-req-push-permission.png 'es-req-push-permission')


### Step 2: Session Initialisation

Before the intervention can be deployed, one needs to add another Micro Dialog widget called `newSessionInit`. This will be a **very short micro dialogue** since it will only contain a single decision point. However, this single decision point will initialise all the important variable for the intervention to run smoothly.

First create a new micro dialog by clicking on the  `New Dialog` button on the bottom of the micro dialog page.

The following rules need to be created **within one decision point**: 

![es-session-init-1](/img/es-session-init-1.png 'es-session-init-1')
<!-- ![Screenshot 2021-04-26 at 19.15.54](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2019.15.54.png) -->

![es-session-init-2](/img/es-session-init-2.png 'es-session-init-2')
<!-- ![Screenshot 2021-04-27 at 15.51.34](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-27%20at%2015.51.34-9531576.png) -->

Rule 1: 
![es-session-init-3](/img/es-session-init-3.png 'es-session-init-3')
<!-- ![Screenshot 2020-11-13 at 22.32.16](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.32.16.png) -->

Rule 2:

Create the variable `$newSessionStartedYear` and assign it to `$systemYear`

![es-session-init-4](/img/es-session-init-4.png 'es-session-init-4')
<!-- ![Screenshot 2020-11-13 at 22.33.17](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.33.17.png) -->

Rule 3: 

Create the variable `$newSessionStartedMonth` and assign it to `$systemMonth`

![es-session-init-5](/img/es-session-init-5.png 'es-session-init-5')
<!-- ![Screenshot 2020-11-13 at 22.33.44](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.33.44.png) -->

Rule 4: 

Create the variable `$newSessionStartedDayOfMonth`and assign it to `$systemDayOfMonth`

![es-session-init-6](/img/es-session-init-6.png 'es-session-init-6')
<!-- ![Screenshot 2020-11-13 at 22.33.57](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.33.57.png) -->

Rule 5:

Create the variable `$newSessionStartedHourOfDay` and assign it to `$systemHourOfDay`

![es-session-init-7](/img/es-session-init-7.png 'es-session-init-7')
<!-- ![Screenshot 2020-11-13 at 22.34.15](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.34.15.png) -->

Rule 6:

Create the variable `$newSessionStartedMinuteOfHour` and assign it to `$systemMinuteOfHour`

![es-session-init-8](/img/es-session-init-8.png 'es-session-init-8')
<!-- ![Screenshot 2020-11-13 at 22.34.34](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.34.34.png) -->

Rule 7:

![es-session-init-9](/img/es-session-init-9.png 'es-session-init-9')
<!-- ![Screenshot 2021-04-27 at 15.51.42](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-27%20at%2015.51.42-9531628.png) -->

This makes it possible to send the first message to the user when he/she opens the app since on the first day the patient could not already set the time when the conversational agent should come back to the patient. 

## Message Groups and Messages Widget

This parts enables the conversational agent to choose one out of three different notifications at random. For instance, it is possible to choose one reminder message out of three different reminder messages randomly. For this go into the `Message Groups and Messages` widget and click on `New Group`. Call this group `1hReminder` and add three different reminder text message like this: 

![es-message-group-1](/img/es-message-group-1.png 'es-message-group-1')
<!-- ![Screenshot 2020-11-13 at 22.23.58](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.23.58.png) -->

The texts could be:

```
Hi $participantName, The session is not yet finished. Would you be so kind as to finish today's session?

Hi $participantName, I am still waiting for your response. Thanks

Hi $participantName, I am still waiting for you. Best.
```

NOTE: Please do not forget to tick the checkbox `This message will only be sent as push notification and NOT appear in the chat`.

![es-message-group-2](/img/es-message-group-2.png 'es-message-group-2')
<!-- ![Screenshot 2021-04-26 at 19.18.35](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2019.18.35.png) -->

## Rules Widget

### Step 1: Add the Platform Rule in the USER INTENSION Rule

When the MobileCoach app is launched on the phone we get a user intention. In this user intention we need to specify if the phone is an android or iOS device. This should look like the following:
![es-rules-1](/img/es-rules-1.png 'es-rules-1')
<!-- ![Screenshot 2020-11-13 at 22.14.10](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.14.10.png) -->

Create a new rule in the `USER INTENSION` section: `$participantIntention` text value equals platform like in the screenshot below: 

![es-rules-2](/img/es-rules-2.png 'es-rules-2')
<!-- ![Screenshot 2021-04-26 at 18.38.53](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2018.38.53.png) -->

Once you have create this first new rule, please add the following **within** the rule. The platform intention need to be set to the following:
![es-rules-3](/img/es-rules-3.png 'es-rules-3')
<!-- ![Screenshot 2020-11-13 at 22.17.06](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.17.06.png) -->

### Step 2: Add the 1-hour reminder rule in the DAILY BASIS RULE

In order to trigger the 1 hour reminder rule what we need to do is to add a rule in the `Daily Basis Rules` just before the day of the intervention is chosen. Note: The `$sendMessagetomorrow` rule might not be there in your intervention as this was a custom thing made by this specific group.

Create the `initialize variables for 1h reminder functionality` rule:

![es-rules-4](/img/es-rules-4.png 'es-rules-4')
<!-- ![Screenshot 2020-11-13 at 22.41.57](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.41.57.png) -->

The rule look like the following: 
![es-rules-5](/img/es-rules-5.png 'es-rules-5')
<!-- [Screenshot 2020-11-13 at 22.42.47](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.42.47.png) -->

This will make sure the variable are initialized properly before starting a new session. The Daily basis rule is run every day at 12:00 AM

The `USER INTENTION` rules should be set accordingly:

![es-rules-ui-1](/img/es-rules-ui-1.png 'es-rules-ui-1')
<!-- ![Screenshot 2020-11-13 at 22.45.12](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.45.12.png) -->

This new rules looks as followed and be named  `initialize variables for 1h reminder functionality` rule:
![es-rules-ui-2](/img/es-rules-ui-2.png 'es-rules-ui-2')
<!-- ![Screenshot 2020-11-13 at 22.46.23](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.46.23.png) -->

### Step 3: Set the PERIODIC BASIS rules

![es-rules-periodic-1](/img/es-rules-periodic-1.png 'es-rules-periodic-1')
<!-- ![Screenshot 2020-11-13 at 22.48.58](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.48.58.png) -->

Here instead of of an execution every 0 minutes something should be run periodically every 5 minutes. This is the goal of this section.

The important variable to set here is the variable `$reminderAfter1hSent`. The rules should look as followed:

![es-rules-periodic-2](/img/es-rules-periodic-2.png 'es-rules-periodic-2')
<!-- <!![Screenshot 2021-04-26 at 19.29.37](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2019.29.37.png) -->



Now you can move on to the second set of rules: 

![es-rules-periodic-3](/img/es-rules-periodic-3.png 'es-rules-periodic-3')
<!-- ![Screenshot 2021-04-26 at 19.34.08](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2019.34.08.png) -->



![es-rules-periodic-4](/img/es-rules-periodic-4.png 'es-rules-periodic-4')
<!-- ![Screenshot 2021-04-26 at 19.29.47](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2019.29.47.png) -->

The idea here is that there should be a maximum one reminder for one session. Before the beginning of a session, the value is set to zero and then, if the reminder is triggered the variable is set to 1. 

The text of the reminder is chosen randomly between three different variable (Please see the Group Message part)

![es-rules-periodic-5](/img/es-rules-periodic-5.png 'es-rules-periodic-5')
<!-- ![Screenshot 2020-11-13 at 22.53.02](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.53.02.png) -->

To compute the amount of time that has passed between the last interaction of the patient with the app we need to set the following rule with the name `calculate $participantInactiveInMinutes`: 

![es-rules-periodic-6](/img/es-rules-periodic-6.png 'es-rules-periodic-6')
<!-- ![Screenshot 2020-11-13 at 22.56.49](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202020-11-13%20at%2022.56.49.png) -->

The javascript code to insert is the following one: 

```javascript
var now = new Date('$systemYear'*1, '$systemMonth'*1-1, '$systemDayOfMonth'*1, '$systemHourOfDay'*1, '$systemMinuteOfHour'*1); 

var lastLogoutDateRaw = '$participantLastLogoutDate'.split(".");
var day = Number(lastLogoutDateRaw[0]);
var month = Number(lastLogoutDateRaw[1])-1;
var year = Number(lastLogoutDateRaw[2]);

var lastLogoutTime = '$participantLastLogoutTime'*1;
var hour = Math.floor(lastLogoutTime);
var min = Math.floor((lastLogoutTime * 60) % 60);

var lastLogout = new Date(year, month, day, hour, min, 0, 0);

var newLessonStarted = new Date('$newSessionStartedYear'*1, '$newSessionStartedMonth'*1-1, '$newSessionStartedDayOfMonth'*1, '$newSessionStartedHourOfDay'*1, '$newSessionStartedMinuteOfHour'*1);

if (newLessonStarted.getTime() > lastLogout.getTime()){
  dt2 = newLessonStarted;
} else {
  dt2 = lastLogout;
}

var diff =(dt2.getTime() - now.getTime()) / 1000;
diff /= 60;
diff = Math.abs(Math.round(diff));

var x = { 'participantInactiveInMinutes' : diff};
x;
```

If this script cannot be copy-pasted please make sure the following variable exist:

```
participantInactiveInMinutes
newSessionStartedMinuteOfHour
newSessionStartedHourOfDay
newSessionStartedMonth
newSessionStartedDayOfMonth
newSessionStartedYear
```

After this the following rules need to be implemented: 

![es-rules-periodic-7](/img/es-rules-periodic-7.png 'es-rules-periodic-7')
<!-- ![Screenshot 2021-04-18 at 15.45.03](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202021-04-18%20at%2015.45.03.png) -->

this window could also look like the following: 

![es-rules-periodic-8](/img/es-rules-periodic-8.png 'es-rules-periodic-8')
<!-- ![Screenshot 2021-04-26 at 19.43.23](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2019.43.23.png) -->

Then create the condition that send a reminder after one hour: 

![es-rules-periodic-9](/img/es-rules-periodic-9.png 'es-rules-periodic-9')
<!-- ![Screenshot 2021-04-26 at 22.42.37](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach_V2.assets/Screenshot%202021-04-26%20at%2022.42.37.png) -->

![es-rules-periodic-10](/img/es-rules-periodic-10.png 'es-rules-periodic-10')
<!-- ![Screenshot 2021-04-18 at 15.49.19](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202021-04-18%20at%2015.49.19.png) -->

![es-rules-periodic-11](/img/es-rules-periodic-11.png 'es-rules-periodic-11')
<!-- ![Screenshot 2021-04-18 at 15.49.28](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202021-04-18%20at%2015.49.28.png) -->

The computation here is: $reminderAfter1hFrequency + 1

Finally do this: 

![es-rules-periodic-12](/img/es-rules-periodic-12.png 'es-rules-periodic-12') 
<!-- ![Screenshot 2021-04-18 at 15.49.46](Tutorial%20to%20Install%20the%201%20hour%20reminder%20on%20MobileCoach.assets/Screenshot%202021-04-18%20at%2015.49.46.png) -->

## Summary

- Step 1: Message groups and messages
  - Implement the messages (Enable the push notification on iOS devices)
  - Randomization (Chose a random reminder/ notification text out of several options )
  - Only a push and not a normal message
- Step 2: Mico dialogues
  - newSessionInit
    - Initialize time variables
    - Initialize session variable “\$interventionStatus” to “session-started”
    - First session push notification
  - Goodbye message
    - Set session variable “\$interventionStatus” to “session-finished”
- Step 3: Execution on a daily basis
  - Sending the “newSessionInit” dialog
- Step 4: Execution on a periodic basis
  - Reset reminder: if session finished, set ​\$reminderAfter1hSent as 0. If session started (i.e ongoing and not done)
    - If the 1 hour reminder is not sent for this session
      - If inactive for > 60 minutes
        - Send reminder
        - Set the ​\$reminderAfter1hSent as 1.
- Step 5: Execution on intention
  - Go intention: send “newSessionInit” for the first session. 