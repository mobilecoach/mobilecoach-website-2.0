---
id: messages
title: Messages
---

If you create/edit a message, you’ll see a lot of possible options. The table below will explain all these options.

![Message options](/img/message-edit.png 'Message options')

| Options | Explanation |
|-------|-------|
| text (with placeholders) | Here you can add the text, that you want to have in the chat. If multiple languages are available, it’s separated with buttons. See all options [HERE](messagetextoptions.md) |
| Integrated media object | You can add here 'local' images, audios, videos, html etc. Look [here](media.md) for more infos. If you want to only link online ressources like videos (youtube, vimeo, etc.) you can use [commands](commands.md). |
| Linked intermediate survey | Not used anymore, will be removed! |
| Message key (must not be unique) | TODO |
| Randomization group | Not used anymore, will be removed! |
| This message is a command (invisible for participants) | If your text is a command you have to check this option. Like show-web 'url' for external links such as videos. See all commands [here](commands.md).
| This message is sticky in the client | You can 'stick' a certain message. This is not used often. |
| This message deactivates all former open questions | With this option, you can deactivate all former open questions. |
| This message expects to be answered by the participant | Check this option if you want to have a response to your message. |
| This message blocks the micro dialog until answered/unanswered | If you want to stop the next message you have to check this option. Then the micro dialog waits until this message is answered. This is mostly checked if you ask the participant something. |
| This message's answer can be cancelled (NO value will be set on cancel) | If the participant is allowed to cancel the answer, you can check this option. Not used often. Better way is to ask the participant before, if he will answer it. Better flow. |
| Answer type | You can select the different answer types here. For example “input text raw” for free text answers. A full list explained you can find [here](answers.md) |
| Answer options | You can add here possible answer options or add text and a placeholder “_” for text/number answers. A complete explanation you can find [here](answers.md) |
| Store message reply to variable (if required) | You can store the answer in a variable. Just select the variable you want to store the answer. (e.g. $participantName for the “What is your name?” question). |
| Store the following value in case of no reply (if required) | If there is no reply, store this value in the variable. |
| Minutes after sending until message is handled unanswered | You can define how long the participant can answer the question. Click on “Infinite” when the question is fully needed. An example could be a quiz, where the participant has only 60 seconds to answer. Here you have to select '1'.  |
| Messages will only be send if the following rules are ALL TRUE | The message will only be send if all of these rule here are true. For instance you can show a message only when the participant age is above 50. See [here](messagerules.md) for examples and options. |
