---
id: answers
title: Answer types / options
---

There are plenty of answer types for the participant. In this section all answer types and their options are explained.

## Answer types

| Answer type | Explanation |
|-------|-------|
| free text | This is a free text input and the text will not be capitalized, so the input “Manuel” would be saved as “manuel”. |
| free text multiline | This free text is a multiline input field, so the participant can add several lines of text. |
| free text raw | This is as well a free text input but the text will be the original one. For instance, “Manuel” would be saved as “Manuel”. |
| free text multiline raw | The multiline input field again, but this time again a one-to-one copy of the text. |
| free numbers | In this field, the participant can only write numbers in it. |
| free numbers raw | In this field, the participant can only write numbers in it. |
| likert | Displayes a likert-scale. Important: you have to provide these answer options. See ZZZ |
| likert silent | Displayes a silent likert-scale. Important: you have to provide these answer options. See ZZZ |
| likert slider | Displayes a likert-scale slider. Important: you have to provide these answer options. See ZZZ |
| select one | The participant can select one from one to several multiple answers. Important: you have to provide these answer options. See ZZZ |
| select one raw | todo |
| select one list | todo |
| select many | The participant can select several from several multiple answers. Important: you have to provide these answer options. See ZZZ |
| select many raw | todo |
| select many modal | todo |
| select one images | Do not use it, needs to be revised!  |
| select many images | Do not use it, needs to be revised!  |
| date | The participant can select a date. |
| time | The participant can select a time. |
| date and time | The participant can select date and time. |
| image | Do not use it, needs to be revised! |
| video | Do not use it, needs to be revised!  |
| audio | Do not use it, needs to be revised!  |
| graphical code | Do not use it, needs to be revised!  |
| custom | Do not use it, needs to be revised!  |

## Answer options

There are several ways you can handle or influence the answer options.

### Placeholders for text / numbers input

You can use a placeholder for all free text/number answer options. If you ask for the name of the participant and the answer option is empty, he/she will only get an empty input field. If you want to handle this more like a normal chat you probably want to have a sentence. The underline symbol “_” is used as a placeholder. Here you can see an example.

![Placeholder example](/img/answer-placeholder-example.png 'Placeholder example')

This text and the placeholder results in the following text bubble.

![Placeholder example bubble](/img/answer-placeholder-bubble.png 'Placeholder example bubble')

### Select One / Many options

If you select the answer types “Select one” or “Select many” you have to predefine the possible answer options. The syntax for that is the following:  
[chat_bubble_text]:[variable_value_to_save]

So if you ask the participant something like “Do you like milk?” there could be the following answer options. The variable_value_to_save is needed that you can save the result in a variable. After that you can check for instance in a rule if $participantLikesMilk = yes / no.

![Select option](/img/answer-select-option.png 'Select option')

These answer options result in the following chat bubbles.

![Select option bubbles](/img/answer-select-option-bubbles.png 'Select option bubbles')

### Likert options

todo
