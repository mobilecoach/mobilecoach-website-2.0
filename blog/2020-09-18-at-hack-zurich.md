---
title: MobileCoach is at HackZurich 2020!
author: Dominik Rüegger
author_title: MobileCoach Team
author_url: https://www.c4dhi.org/team/
author_image_url: https://www.c4dhi.org/wp-content/uploads/2018/09/dominik-rueegger.png
tags: [mobilecoach, hackzurich]
---

We invite the HackZurich participants to use MobileCoach for their prototype!

The following resources are relevant for you:

[app repository for Elena+](https://bitbucket.org/mobilecoach/elena-covid19-hackathon/src/master/)

[instruction videos for the chat-interaction designer](https://vimeo.com/channels/1544042)

[tutorials](https://www.mobile-coach.eu/docs/tutorials/webdesigner/overview/)

