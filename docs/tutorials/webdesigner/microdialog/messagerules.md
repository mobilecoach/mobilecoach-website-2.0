---
id: messagerules
title: Message rules
---

In messages you can add rules. These rules have to be TRUE that the message is shown. So for instance if you only want to show this message if participant is older than 50, you could solve that with these message rules.
You can also add several rules. ALL! of these rules must be true. There is a list of all [rule conditions](../rules/ruleconditions.md).

## Example

Let’s take up the “Do you like milk?” question from the [answer options](answers.md) again.. If you want to show a message when the participant answered it with “I like it! 😄” you can do it like this:

Go to the message and add a new rule (at the bottom). Now you can create your rule that’s based on a condition. So IF x = y THEN send message. So the needed rule could look like the following:

![Message rules example](/img/message-rules.png 'Message rules example')
