---
id: regularexpressions
title: Regular Expressions
---

For text operation rules you can also use regular expressions. We mostly use regular expressions (Regex) for input-validation.

You can find online many Regex tutorials and ALWAYS check your Regex online. E.g. here: [Regexpal.com](http://regexpal.com)

A very good Regex Cheat Sheet: <https://cheatography.com/davechild/cheat-sheets/regular-expressions/>

Here are some examples:

* a|b|c|d
* 1|2|3|4|5
* [0-9]+
* [a-zA-Z]*
* [a-z]{2,4}
* \d+
