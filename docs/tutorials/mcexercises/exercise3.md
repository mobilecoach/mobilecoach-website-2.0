---
id: exercise3
title: MobileCoach Exercise 3
---
# Images, Timed-answers, and Calculations

In this third exercise, you will integrate an image, a short quiz, and a calculation into a dialog. If you get stuck, use the documentation, the video tutorials for intervention authors or the MobileCoach user forum (see links at the end).

**Step 1:** Create a new Micro Dialog "MCAssignment3". Go to the Rules tab, expand the "Execution on USER INTENTION" tree and click edit on the rule "$participantIntention text value equals go". Select your created Micro Dialog from the dropdown "Micro Dialog to start".

**Step 2:** Create a new variable "$points" in the Variable tab. The default value should be 0. We will use this variable for the calculation.

**Step 3:** Harry/Sally is eager to send you a picture and asks about your opinion. The participant has to answer within 60 seconds to provide his/her opinion about the picture. Thus, please add a short message that explains the "time-to-answer" details to the participant (e.g. "I will show you a picture and ask you something about it. You have then 60 seconds for your answer."). Moreover, please add the following answer options (e.g. "Ok, I guess?", "Fantastic image!") and save the selected answer in a new variable. Please also store in the very same variable the "noAnswer" if the participant does not answer within 60 seconds. Please set the response time to 60 seconds, respectively.

**Step 4:** Depending on the participant's response, the $points variable will need to be changed. Add a decision point, in which $points will be incremented by 20 (for the "Ok, I guess?" answer), by 100 (for the "Fantastic image!" answer) or do nothing (in case no answer was provided).

**Step 5:** Harry/Sally should finally communicate a message with the points depending on the two answer options / whether an answer was provided. For example, if the participant has not answered the question, the message could be "Oops, that was too late, $points points, sorry.".

### Helpful links/material
-	Documentation for Intervention Implementations with MobileCoach: <https://www.mobile-coach.eu/docs/tutorials/webdesigner/overview/>
-	MobileCoach User Forum (e.g., for questions and discussions, peer help support): <https://discourse-mobilecoach.ch>
-	MobileCoach Designer video tutorials for intervention authors: <https://vimeo.com/channels/1809334>
