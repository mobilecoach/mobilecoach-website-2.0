---
id: exercise1
title: MobileCoach Exercise 1
---
#  Variables

In this first exercise, you will create a short "Onboarding" dialogue in your intervention. MobileCoach Designer is used for the implementation of the logic and conversational turns of your digital health intervention. In order to complete this assignment, you have to carefully watch the first 3 MobileCoach video tutorials: <https://vimeo.com/channels/1809334>
Please implement the following dialogue and add one additional conversational turn of your choice:

> "Hi I'm $coach, your digital coach."
> "What is your name?" -> free text answer option -> saving the response in the $participantName variable
> "Hi $participantName, nice to meet you!"


**Hint 1:** Use an underline "_" as a placeholder for your answer. Figure 1 shows you how the result of this assignment should look like.

**Hint 2:** Save input text as “free text raw” so capitalized letters in names are taken into account.


**Figure 1. Example of a quick and dirty "Onboarding" dialogue**
![exercise1](/img/exercise1.gif 'exercise1')


### Helpful links/material
-	Documentation for Intervention Implementations with MobileCoach: <https://www.mobile-coach.eu/docs/tutorials/webdesigner/overview/>
-	MobileCoach User Forum (e.g., for questions and discussions, peer help support): <https://discourse-mobilecoach.ch>
-	MobileCoach Designer video tutorials for intervention authors: <https://vimeo.com/channels/1809334> (hint: the video tutorials 1-3 are the most helpful for this assignment) 