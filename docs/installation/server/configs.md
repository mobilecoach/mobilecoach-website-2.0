---
id: configs
title: Configuration
---

## **Introduction**

This manual deals with the MC Server configuration file and its variables.  
You can find the file in the following path: ```/mobilecoach-server/mc_global/mc_data/configuration.properties```.  
The basic configuration of the server is shown below and all variables are explained briefly.

## **Configuration**

### Caching

**cachingActive**  
Description to be displayed

Default value: *true*
___

### Media linking URL

**mediaObjectLinkingBaseURL**  
Is used if you want to add images or videos directly in the mobile-coach designer.
The correct syntax is as following: **https://[HOSTNAME]/MC/files-short/**

Default value: *'https://f.mobile-coach.eu/'*

___

### Surveys - not used

:::caution
The surveys are not used by now!  
Use LimeSurvey instead.
:::

**listOpenScreenSurveysOnBaseURL**  
Not used by now.

Default value: *-*

**surveyLinkingBaseURL**  
Not used by now.

Default value: *-*

**acceptedStopWordsSeparatedByComma**  
Not used by now.

Default value: *-*
___

### Admin configuration

These credentials needs to be changed after the setup in the webApp.  
The Admin username and password are stored in the database.

**defaultAdminUsername**  
Username for the webApp Login

Default value: *admin*

**defaultAdminPassword**  
Password for the webApp Login

Default value: *admin*
___

### Automatic login

:::danger
CAUTION! DO NEVER ACTIVATE THIS ON PUBLIC SERVERS! IT'S ONLY FOR TESTING!
:::

**automaticallyLoginAsDefaultAdmin**  
If the value is true, you don't have to login in the mobilecoach web designer.
Never activate this on public servers!

Default value: *false*
___

### Language settings

The available languages for interventions.  
Based on the choice during onboarding, in a client, either the German or French dialogs can be sent.  
These are programmed in parallel.

**adminLocale**  
The language of the webApp

Default value: *en-GB*  
Possible values: *de-CH, fr-CH, en-GB, en-US*

**interventionLocales**  
Which languages can be used in the micro messages.

Default value: *de-CH,fr-CH*  
Possible values: *de-CH, fr-CH, en-GB, en-US*
___

### Paths

Please don't change it unless you are testing a development system.  
Docker-based setup uses these locations by default.  
Changing it would require changes in docker-compose files.

**loggingFolder**  
Folder with the logs

Default value: */mc_data/logs*

**storageFolder**  
Folder for file storage

Default value: */mc_data/FileStorage*

**mediaUploadFolder**  
Folder for the media upload

Default value: */mc_data/MediaUpload*

**mediaCacheFolder**  
Folder for the media cache

Default value: */mc_data/MediaCache*

**templatesFolder**  
Folder for the templates

Default value: */mc_data/templates*
___

### Logging levels

The logging levels can be set in these variables.

**loggingConsoleLevel**  
Description

Default value: *DEBUG*  
Possible values: *to be displayed*

**loggingRollingFileLevel**  
Description

Default value: *WARN*  
Possible values: *to be displayed*
___

### Export, History, MediaUpload Security

Description todo

**fileExtension**  
The file extension for the intervention when it is exported.

Default value: *.mc*

**maxVariableHistory**  
Number of times a variable can store its past value

Default value: *1000*

**mediaUploadSecurityCheck**  
Description

Default value: *true*
___

### Survey listing configuration - not used

:::caution
The surveys are not used by now!  
Use LimeSurvey instead.
:::

**surveyListingTitle**  
Not used by now.

Default value: *Active surveys:*

**surveyListingNoneActive**  
Not used by now.

Default value: *No survey active.*

**surveyListingNotActive**  
Not used by now.

Default value: *Survey listing inactive.*
___

### Statistics configuration

Description todo

**statisticsFileEnabled**  
Description

Default value: *true*

**statisticsFile**  
Description

Default value: */mc_data/statistics.properties*
___

### Simulation configuration

:::danger
CAUTION! DO NEVER ACTIVATE THIS ON PUBLIC SERVERS! IT'S ONLY FOR TESTING!
:::

**simulatedDateAndTime**  
If set to "true" while testing, time can be simulated and the rules can be tested based on the simulated time.

Default value: *false*
___

### Database configuration

The mongodb database configuration for tomcat to connect to and store the data.

**databaseHost**  
Hostname of the db

Default value: *mongodbservice*

**databasePort**  
Used port for the communication

Default value: *27017*

**databaseUser**  
Username of the db

Default value: *mc*

**databasePassword**  
Password of the db

Default value: *mc*

**databaseName**  
Name of the mongoDB db

Default value: *mc*
___

### General mailing configuration

The general mailing configuration.  
This and the section below is required to send emails from the server.

**mailhostIncoming**  
Description

Default value: *localhost*

**mailboxProtocol**  
Used protocol to send and receive mails.

Default value: *pop3*  
Possible values: *pop3, smtp*

**mailboxFolder**  
Description

Default value: *INBOX*

**mailhostOutgoing**  
Description

Default value: *localhost*

**mailUser**  
Description

Default value: **

**mailPassword**  
Description

Default value: **
___

### Email configuration

Settings for sending emails during an intervention.

**emailActive**  
Needs to be "true" for the server to send emails.

Default value: *false*

**emailFrom**  
Needs to be set correctly based on the domain for the recipients to know who sent the emails.

Default value: *a@b.eu*

**emailSubjectForParticipant**  
Description

Default value: *MobileCoach Message*

**emailSubjectForSupervisor**  
Description

Default value: *MobileCoach Supervisor Notification*

**emailSubjectForTeamManager**  
Description

Default value: *MobileCoach Team Manager Notification*

**emailTemplateForTeamManager**  
Description

Default value: *Hi Team Manager of $participantName!\n\nYou have one or more new messages from your participant, which can be read on the MobileCoach dashboard.*
___

### SMS configuration

SMS can be sent via ASPSMS or TWILO service. In ASPSMS (<https://www.aspsms.com/en/login.asp),> a new phone number can be ordered or an existing one can be used. It is set in the variable "smsPhoneNumberFrom". Please use the phone number along with the full country extension. Example. 0041766xxxxxx would be a valid format. From the ASPSMS account, we can also get the "smsUserKey" and "smsUserPassword".

**smsActive**  
Description

Default value: *false*

**smsServiceType**  
Description

Default value: *ASPSMS*  
Possible values: *ASPSMS, TWILIO*

**smsMailSubjectStartsWith**  
Description

Default value: *SMS received on*

**smsUserKey**  
Key for ASPSMS or Account SID of TWILIO

Default value: *-*

**smsUserPassword**  
Account Password for ASPSMS or Primary Token for TWILIO

Default value: *-*

**smsPhoneNumberAcceptedCountryCodes**  
Description

Default value: *41,43,49*

**smsPhoneNumberCountryCorrection**  
Description

Default value: *41*

**smsPhoneNumberFrom**  
Description

Default value: *-*
___

### Deepstream configuration

Description todo

**deepstreamActive**  
Needs to be true for the server to send messages as chat in the MobileCoach client.

Default value: *true*

**deepstreamHost**  
Is set automatically during the docker based server setup process.

Default value: *wss:///<hostname/>:/<port_deepstream/>*

**deepstreamMinClientVersion**  
Not used by now!

Default value: *1*

**deepstreamMaxClientVersion**  
Not used by now!

Default value: *1*
___

### Push notifications configuration ANDROID

**pushNotificationsActive**  
Needs to be true for the server to send push notifications MobileCoach client running on a real device (iOS or Android).

Default value: *false*

**pushNotificationsAndroidActive**  
Needs to be true for the following android settings to take.

Default value: *false*

**pushNotificationsAndroidEncrypted**  
If set true, the push notifications are encrypted when sent via Firebase cloud.

Default value: *false*

**pushNotificationsAndroidAuthKey**  
Can be obtained when setting up push notifications for the client in Firebase.

Default value: *-*
___

### Push notifications configuration IOS

Push notifications in iOS have dual modes.  
Production mode and development mode.
The variables pushNotificationsProductionMode, pushNotificationsIOSCertificateFile and pushNotificationsIOSCertificatePassword change based on that.

**pushNotificationsActive**  
Needs to be true for the server to send push notifications MobileCoach client running on a real device (iOS or Android).

Default value: *false*

**pushNotificationsIOSActive**  
Needs to be true for the following iOS settings to take.

Default value: *false*

**pushNotificationsIOSEncrypted**  
If set true, the push notifications are encrypted.

Default value: *false*

**pushNotificationsProductionMode**  
Description

Default value: *false*

**pushNotificationsIOSAppIdentifier**  
The unique app id of the iOS

Default value: *com.mycompany.myapp*

**pushNotificationsIOSCertificateFile**  
Description

Default value: */mc_data/certs/push-http2.p12*

**pushNotificationsIOSCertificatePassword**  
Description

Default value: *abc123topsecret*
___
