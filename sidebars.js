module.exports = {
  docs: [
    {
      type: 'category',
      label: 'About',
      items: [
        'about/what-is-mc',
        'about/projects',
        'about/publications',
        'about/team',
        'about/repositories',
        'about/contact',
        'about/imprint'
      ]
    },
    {
    type: 'category',
    label: 'Setup',
    items: [
      {
        'Server infrastructure': ['installation/server/setup', 'installation/server/setup-one-port', 'installation/server/configs',],
      },
      {
        'MC Mobile App': ['installation/mobileapp/setup', 'installation/mobileapp/configs',],
      },
      'installation/pushnotification',
      'installation/limesurvey'
    ]
  },
  {
    type: 'category',
    label: 'Tutorials',
    items: [
      {
        'MC Web Designer': [
           'tutorials/webdesigner/overview',
           {
            'Micro Dialogs': [
              'tutorials/webdesigner/microdialog/messages',
              'tutorials/webdesigner/microdialog/messagetext',
              'tutorials/webdesigner/microdialog/commands',
              'tutorials/webdesigner/microdialog/media',
              'tutorials/webdesigner/microdialog/answers',
              'tutorials/webdesigner/microdialog/messagerules',
              'tutorials/webdesigner/microdialog/decisionpoint'
            ]
           },
           {
             'Rules': [
               'tutorials/webdesigner/rules/mainruletree',
               'tutorials/webdesigner/rules/userintentions',
               'tutorials/webdesigner/rules/ruleconditions',
               'tutorials/webdesigner/rules/regularexpressions'
             ]
           },
           'tutorials/webdesigner/accesscontrol',
           'tutorials/webdesigner/exportdata',
           {
            'UseCases': [
              'tutorials/webdesigner/usecases/escalationmechanism',
            ]
          },
        ]
      },
      {
        'MC Mobile App': [
          'tutorials/mobileapp/overview',
          'tutorials/mobileapp/mainfeatures',
          'tutorials/mobileapp/coming',
          {
            'UseCases': [
              'tutorials/mobileapp/usecases/designchanges',
              'tutorials/mobileapp/usecases/appicons',
              'tutorials/mobileapp/usecases/typespeed',
              'tutorials/mobileapp/usecases/onboarding',
              'tutorials/mobileapp/usecases/testandroid',
              'tutorials/mobileapp/usecases/testios',
              'tutorials/mobileapp/usecases/googlefit',
              'tutorials/mobileapp/usecases/healthkit',
              'tutorials/mobileapp/usecases/prodandroid',
              'tutorials/mobileapp/usecases/prodios'
            ]
          },
        ],
      },
      {
        'MC Exercises': [
          'tutorials/mcexercises/overview',
          'tutorials/mcexercises/exercise1',
          'tutorials/mcexercises/exercise2',
          'tutorials/mcexercises/exercise3',
          'tutorials/mcexercises/exercise4',
        ]
      },
    ]
  }
]
};
