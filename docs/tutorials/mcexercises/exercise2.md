---
id: exercise2
title: MobileCoach Exercise 2
---
# Three-Hypen Technique, Conditional Messages, and Control Structures

In this second exercise, you will create a slightly more complex "Onboarding" Micro Dialog. The goal is to get to know MobileCoach Designer better, for example, its rule mechanics, user intentions, or variable management. If you get stuck, use the documentation, the recorded video tutorials or the MobileCoach User Forum.

**Step 1:** Create a new Micro Dialog "MCAssignment2". Go to the Rules tab, expand the "Execution on USER INTENTION" tree and click edit on the rule "$participantIntention text value equals go". Select your newly created Micro Dialog on the dropdown "Micro Dialog to start".

**Step 2:** The Micro Dialog with Harry/Sally should start with at least two conversational turns (small talk / saying hello / how are you / etc.). Create two messages with “select one” answer types, that block the Micro Dialog and need to be answered by the participant. Use at least once the triple-hyphen technique (---) in the message to separate one message into two speech bubbles.

**Step 3:** Then, Harry/Sally should ask for the name of the participant (use "free text raw" as answer type) and the answer should be stored in the variable $participantName

**Step 4:** Then, Harry/Sally should use the answer (i.e. the name) in a message and ask the participant whether the name is correct (yes, no answer option). A Decision Point should be used to ask the participant to correct the name in case it was wrong. If the name was correct, then Harry/Sally should say: Thanks a lot and have a nice day! The participant should use an emoticon as a very last answer to the goodbye message.

**Step 5:** Additionally, you should create a Decision Point as a very last message that allows you to jump back to the very first message. This should help you work on this assignment more efficiently. Note (very important): This Decision Point must always be linked to a message above that decision point so that at least one conversational turn (i.e. a message that needs to be answered) is implemented in between that linked message and Decision Point. Having no conversational turn in-between the message and the Decision Point results in an endless loop and the exercise cannot be completed at that moment.

### Helpful links/material
-	Documentation for Intervention Implementations with MobileCoach: <https://www.mobile-coach.eu/docs/tutorials/webdesigner/overview/>
-	MobileCoach User Forum (e.g., for questions and discussions, peer help support): <https://discourse-mobilecoach.ch>
-	MobileCoach Designer video tutorials for intervention authors: <https://vimeo.com/channels/1809334> (hint: the video tutorials 1-4 are the most helpful for this assignment) 
