---
id: exercise4
title: MobileCoach Exercise 4
---
# Multiple-Day Coaching and Date & Time Simulator

In this fourth exercise, you will integrate a Three-Day intervention that provides users with a daily health-related tip. You will also learn how to travel in time with the Date & Time Simulator. If you get stuck, please use the following video tutorial: <https://vimeo.com/402344259>

**Step 1:** Create three Micro Dialogues “Session 1”, “Session 2”, and “Session 3”. Each Session should start with a welcome dialog, follow up with a health-related tip of the day, and finish with a goodbye dialog.

**Step 2:** Create the two Variables $session (initialized as 1) and $interventionStatus (initialized as session-started) in the “Variables” tab. Next, create the following decision point at the end of Sessions 1 and 2: “$interventionStatus text value equals session-finished”. The result of the variable should be stored in $interventionStatus.

**Step 3:** In the “Rules” tab, go to the “Execution on DAILY BASIS” rules. When the $interventionStatus text value equals session-finished, the following rule should increment the session by one: “$session + 1 calculate value but result is always true”. This incremented session number should be stored in the variable $session. An additional rule, “$session calculated value equals 2”, should trigger the micro dialog to start “Session 2”. Therefore, tick the box “Start micro dialog if result is TRUE” and set Session 2 as the micro dialog to start. Analogously, create another rule for session 3. We also want to send out reminder messages during the day, so you might want to change the time slider to around 10:00 am (see screenshot below)

![exercise4_1](/img/exercise4_1.png 'exercise4_1')

**Step 4:** Go back to the “Micro Dialogs” tab and add the following decision point at the beginning of Session 2 and Session 3: ”$interventionStatus text value equals session-started” with the result stored in the Variable $interventionStatus. In the final third Session, add the decision point “$interventionStatus text value equals intervention-finished”, again with the result stored in the Variable $interventionStatus.

In “Execution on DAILY BASIS” in the “Rules” tab, now please add the rule “$interventionStatus text value equals intervention-finished” and tick the following box: “Stop current rule execution run and finish intervention for this participant if result is TRUE”.

![exercise4_2](/img/exercise4_2.png 'exercise4_2')

**Step 5:** We are now finally ready to start time-traveling! Start your intervention by clicking on the “Activate Monitoring” button. When Session 1 is finished, go to the “Basic Settings and Modules” Tab and click the button Jump on DAY to the Future!” of the DATE and TIME SIMULATOR. Finally, finish the intervention by using the DATE and TIME SIMULATOR.

### Helpful links/material
-	Documentation for Intervention Implementations with MobileCoach: <https://www.mobile-coach.eu/docs/tutorials/webdesigner/overview/>
-	MobileCoach User Forum (e.g., for questions and discussions, peer help support): <https://discourse-mobilecoach.ch>
-	MobileCoach Designer video tutorials for intervention authors: <https://vimeo.com/channels/1809334>
